import axios from "axios";
const BASE_URL = "https://breakingbadapi.com/api/";

function getCharacters() {
    return axios
    .get(BASE_URL + "characters")
    .then(response => {
        return response.data;
    })
    .catch(error => {
        document.getElementById('root').innerHTML("Se ha producido un error en la llamada a la API" + error);
    });
}

export {getCharacters};