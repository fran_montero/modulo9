import "./styles.css";
import * as DataBusiness from "./data-business.js";
import * as Utils from "./utils.js";

DataBusiness.getCharacters().then(datos => {
    const datosPersonajes = [];
    document.getElementById('root').innerHTML = "";
    for (let characterCount of datos) {
        const node = Utils.createCharacterRow(characterCount);
        node.onclick = function() {
            Utils.showCharacter(characterCount);
        };
        datosPersonajes.push(node);
    } 
    console.log(datosPersonajes);
    for (let personaje of datosPersonajes) {
        document.getElementById('root').append(personaje);
    }
});
